import styled from "styled-components";
import { Typography } from "antd";

const { Title } = Typography;

const Background = styled.div`
  width: auto;
  height: 100dvh;
`;

const TextContainer = styled.div`
  position: fixed;
  top: 10rem;
  left: 100px;
  padding: 2rem 2rem;
  background: rgba(255, 255, 255, 0.75);
`;

export const MaintainPage: React.FC = () => {
  return (
    <Background>
      <TextContainer>
        <Title>Venus.AI is in maintainace mode!</Title>

        <Title level={3}>
          Our femboy admin is going on a long trip to find himself. We will open back on Monday when
          he's back.
        </Title>

        <Title level={3}>
          Enjoy your weekend with your family and touch grass. Remember, there is no free proxy.
        </Title>

        <Title level={3}>
          To our Patrons, you can still contact me if you want to refund. We will open the site back
          on Monday.
        </Title>

        <p className="mb-0 mt-8">
          <a target="_blank" href="https://www.reddit.com/r/VenusAI_Official">
            <img src="https://img.icons8.com/bubbles/50/null/reddit.png" alt="reddit" />
          </a>
          <a target="_blank" href="https://discord.gg/wFPemXeEUf">
            <img src="https://img.icons8.com/bubbles/50/null/discord-logo.png" alt="discord" />
          </a>
        </p>
      </TextContainer>
    </Background>
  );
};
