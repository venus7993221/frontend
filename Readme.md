# Project

Use `yarn dlx @yarnpkg/sdks vscode` to use TS in VS Code

# How to run

1. Update `VITE_SP_ENDPOINT`, `VITE_SP_ANON_KEY`, `VITE_BUCKET_URL`, `VITE_BACKEND_ENDPOINT` with the correct value
2. For `VITE_SP_ANON_KEY`, use the `anon key`, DO NOT use `service_role key`
2. Run `yarn start:dev` to start
