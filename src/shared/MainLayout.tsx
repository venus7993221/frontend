import { useContext } from "react";
import { Link, Outlet } from "react-router-dom";
import { Layout, Menu, Spin } from "antd";
import { SearchOutlined, UserAddOutlined } from "@ant-design/icons";

import { AppContext } from "../appContext";

import { UserAvatar } from "./components/UserAvatar";
import { EnvIndicator } from "./components/EnvIndicator";
import { useMobileDetect } from "../hooks/useMobileDetect";

const { Header, Content, Footer } = Layout;

export const MainLayout: React.FC = () => {
  const { session, profile, isProfileLoading } = useContext(AppContext);
  const { isMobile } = useMobileDetect();

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Header>
        <Menu selectable={false} theme="dark" mode="horizontal">
          <Menu.Item className="no-padding" key="home">
            <Link to="/">
              <EnvIndicator />
              <img
                style={{ width: "100px", position: "relative", top: "-1px" }}
                src="/logo_small.png"
                alt="logo"
              />
            </Link>
          </Menu.Item>
          <Menu.Item key="search">
            <Link to="/search">
              <SearchOutlined /> Search
            </Link>
          </Menu.Item>

          {(() => {
            if (!session) {
              return (
                <>
                  <Menu.Item style={{ marginLeft: "auto" }} key="login">
                    <Link to="/login">Login</Link>
                  </Menu.Item>
                  <Menu.Item key="register">
                    <Link to="/register">Register</Link>
                  </Menu.Item>
                </>
              );
            }

            if (isProfileLoading) {
              return (
                <Menu.Item style={{ marginLeft: "auto" }}>
                  <Spin />
                </Menu.Item>
              );
            } else if (profile) {
              return (
                <>
                  {!isMobile && (
                    <Menu.Item key="create" style={{ marginLeft: "auto" }}>
                      <Link to="/create_character">
                        <UserAddOutlined /> Create Character
                      </Link>
                    </Menu.Item>
                  )}

                  <UserAvatar />
                </>
              );
            }
          })()}
        </Menu>
      </Header>

      <Content className="main-layout-content">
        <Outlet />
      </Content>

      <Footer style={{ textAlign: "center" }}>
        <p>
          <strong>Venus AI</strong> - Chat with your waifu/husbando. Uncensored, No Ad, Forever
          Free.
        </p>

        <span>
          <span> - </span>
          <a href="/policy" target="_blank">
            📜 Content & Private Policy
          </a>
          <span> - </span>
          <a href="/term" target="_blank">
            🤝 Term of Use
          </a>
          <span> - </span>
          <a href="/faq" target="_blank">
            🙋 FAQ
          </a>
        </span>
      </Footer>
    </Layout>
  );
};
